import React, {Component} from "react";
import PlantationDetail from "../SharedComponents/PlantationDetail";
import NavBar from "../Layouts/NavBar";
import Footer from "../Layouts/Footer";
import HomePageArticles from "./HomePageArticles";
import HomePageModal from "./HomePageModal";

class Home extends Component{
    constructor(props){
        super(props);

    }
    render(){
        return(
            <>
            <NavBar/>
            <PlantationDetail/>
            <HomePageArticles/>
            <Footer/>
            </>
        );
    }
}
export default Home;
