import React, { Component } from "react";
import StarRatings from "react-star-ratings";

class HomePageArticles extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {

    }
    render() {
        return (
            <>
                <div style={{ padding: "7%", display: "flex" }}>
                    <div style={{ flex: "65%", display: "flex", flexDirection: "column", alignItems: "baseline", padding: "2% 5% 0 5%", }}>
                        <div style={{ display: "flex", width: "100%",alignItems:"baseline" }}>
                            <h4>Articles</h4>
                            <hr style={{ backgroundColor: "lightGrey", width: "85%", }} />
                        </div>
                        <div style={{ display: "flex", justifyContent:"space-around"}}>
                            <div style={{width:"30%", display: "flex", flexDirection: "column", minHeight: "200px", marginBottom: "10px", boxShadow: "1px 2px 2px 1px lightGrey", borderRadius: "5px" }}>
                                <div style={{ width: "100%" }}><img style={{ width: "100%", height: "100%" }} src={require("../../media/eg.png")} /></div>
                                <div style={{
                                    display: "flex", flexDirection: "column", flex: "70%",
                                    padding: " 10%"
                                }}>
                                    <span style={{ fontWeight: "bold", fontSize: "14" }}>The Coca-Cola Plantation</span>
                                    <date>12-sep-2019</date>
                                    <span className="font-small color-light">sint occaecat cupidatat non proident, 
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                </div>
                            </div>
                            <div style={{ display: "flex", flexDirection: "column",width:"30%", minHeight: "200px", marginBottom: "10px", boxShadow: "1px 2px 2px 1px lightGrey", borderRadius: "5px" }}>
                                <div style={{ width: "100%" }}><img style={{ width: "100%", height: "100%" }} src={require("../../media/eg.png")} /></div>
                                <div style={{
                                    display: "flex", flexDirection: "column", flex: "70%",
                                    padding: " 10%"
                                }}>
                                    <span style={{ fontWeight: "bold", fontSize: "14" }}>The Coca-Cola Plantation</span>
                                    <date>12-sep-2019</date>
                                    <span className="font-small color-light">sint occaecat cupidatat non proident, 
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                </div>
                            </div>
                            <div style={{ display: "flex", flexDirection: "column",width:"30%", minHeight: "200px", marginBottom: "10px", boxShadow: "1px 2px 2px 1px lightGrey", borderRadius: "5px" }}>
                                <div style={{ width: "100%" }}><img style={{ width: "100%", height: "100%" }} src={require("../../media/eg.png")} /></div>
                                <div style={{
                                    display: "flex", flexDirection: "column", flex: "70%",
                                    padding: " 10%"
                                }}>
                                    <span style={{ fontWeight: "bold", fontSize: "14" }}>The Coca-Cola Plantation</span>
                                    <date>12-sep-2019</date>
                                    <span className="font-small color-light">sint occaecat cupidatat non proident, 
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div style={{ flex: "35%", padding: "0 2% 0 2%", display: "flex", boxShadow: "1px 1px 1px 1px lightgrey", borderRadius: " 5px", flexDirection: "column" }}>
                        <div style={{ display: "flex", justifyContent: "space-between", padding: "10px 0px 10px 0px" }}>
                            <h4>Latest Plantations</h4>
                            <a style={{ cursor: "pointer" }}>Veiw All</a>
                        </div>

                        <div style={{ display: "flex", height: "80px", marginBottom: "10px", boxShadow: "1px 2px 2px 1px lightGrey", borderRadius: "5px" }}>
                            <div style={{ flex: "30%" }}><img style={{ width: "100%", height: "100%" }} src={require("../../media/eg.png")} /></div>
                            <div style={{
                                display: "flex", flexDirection: "column", flex: "70%",
                                padding: " 2%"
                            }}>
                                <span style={{ fontWeight: "bold", fontSize: "14" }}>The Coca-Cola Plantation</span>
                                <StarRatings
                                    rating={4}
                                    starRatedColor="var(--primary)"
                                    numberOfStars={5}
                                    name="rating"
                                    starDimension={"14px"}
                                    starSpacing={"2px"}
                                />
                                <span className="font-small color-light">Planted Trees : 1234</span>
                            </div>
                        </div>
                        <div style={{ display: "flex", height: "80px", boxShadow: "1px 2px 2px 1px lightGrey", borderRadius: "5px" }}>
                            <div style={{ flex: "30%" }}><img style={{ width: "100%", height: "100%" }} src={require("../../media/eg.png")} /></div>
                            <div style={{
                                display: "flex", flexDirection: "column", flex: "70%",
                                padding: " 2%"
                            }}>
                                <span style={{ fontWeight: "bold", fontSize: "14" }}>The Coca-Cola Plantation</span>
                                <StarRatings
                                    rating={4}
                                    starRatedColor="var(--primary)"
                                    numberOfStars={5}
                                    name="rating"
                                    starDimension={"14px"}
                                    starSpacing={"2px"}
                                />
                                <span className="font-small color-light">Planted Trees : 1234</span>
                            </div>
                        </div>

                    </div>
                </div>
            </>
        );
    }
}
export default HomePageArticles;