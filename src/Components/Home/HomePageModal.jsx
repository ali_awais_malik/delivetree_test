import React, { Component } from "react";
import PlantationDetail from "../SharedComponents/PlantationDetail";
import Home from "./Home";

class HomePageModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
                modalDisplay:"flex",
        }
    }
    render() {
        const img = ['../../media/cocacola.png', '../../media/gov.png', '../../media/hbl.png'];
        const img2 = ["../../media/co2.png", "../../media/tree.png", "../../media/o2.png"];
        return (
            <> 
                <div style={{ display: `${this.state.modalDisplay}`,position:"absolute", flexDirection: "column",height:"100vh", margin: "2% 2% 2% 2%", opacity: "1", backgroundColor: "white", backgroundColor: "rgba(255, 255, 255, 0.9)", height: "calc(100vh - 60px)", overflow: "hidden" }}>
                    <div style={{ margin: "30px", display: "flex", alignSelf: "flex-end", cursor:"pointer", fontSize:18 }} onClick={()=>{this.setState({modalDisplay:"none"})}}><p>X</p></div>
                    <div style={{ display: "flex", width: "100%", padding: "0% 2% 10% 2%" }}>
                        <div style={{ flex: "50%", textAlign: "center", padding: "0 5%", borderRight: "1px solid lightgrey" }}>
                            <h4 style={{ fontSize: "21px" }}>
                                A change for the Environment
                    </h4>
                            <p style={{ fontSize: 16, padding: "25px 5%" }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur.
                    </p>
                            <p style={{ fontSize: 16, padding: "0 5% 2% 5%" }}>
                                sint occaecat cupidatat non proident,
                                sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                            <div style={{ display: "flex", justifyContent: "space-evenly" }}>
                                <button className="custom-button">
                                    BE A PART
                        </button>
                                <button className="link-like-button">
                                    LEARN MORE
                        </button>
                            </div>
                        </div>
                        <div style={{ flex: "50%", textAlign: "center", padding: "0 5%" }}>
                            <h4 style={{ fontSize: "21px" }}>Official Partners</h4>
                            <div style={{ display: "flex", justifyContent: "space-around", fontSize: 14, padding: "25px 5% 5% 5%" }}>
                                {
                                    img.map(src => <img src={require('../../media/gov.png')} style={{ marginBottom: "12px", borderRadius: "4px", width: "25%" }} />)
                                }

                            </div>
                            <div style={{ display: "flex", justifyContent: "space-around", fontSize: 14, padding: "0 10%" }}>
                                <hr style={{ backgroundColor: "lightGrey", width: "100%" }} />
                            </div>
                            <div style={{ display: "flex", justifyContent: "space-around", fontSize: 14, padding: "25px 5% 5% 5%" }}>
                                <div style={{ flex: "20%", display: "flex", flexDirection: "column", alignItems:"center" }}>
                                    <img src={require("../../media/co2.png")} style={{ borderRadius: "4px", width: "70%" }} />
                                    <span style={{ fontSize: 16, fontWeight: "bold" }}>1222</span>
                                    <span className="font-small color-light">tonnes</span>
                                    <span>Carbon</span>
                                    <span>Reduced</span>
                                </div>
                                <div style={{ flex: "20%", display: "flex", flexDirection: "column", alignItems:"center" }}>
                                    <img src={require("../../media/tree.png")} style={{ borderRadius: "4px", width: "70%" }} />
                                    <span style={{ fontSize: 16, fontWeight: "bold" }}>1222</span>
                                    <span className="font-small color-light">millions</span>
                                    <span>Tree</span>
                                    <span>Planted</span>
                                </div>
                                <div style={{ flex: "20%", display: "flex", flexDirection: "column", alignItems:"center" }}>
                                    <img src={require("../../media/o2.png")} style={{ borderRadius: "4px", width: "70%" }} />
                                    <span style={{ fontSize: 16, fontWeight: "bold" }}>1222</span>
                                    <span className="font-small color-light">tonnes</span>
                                    <span>Oxygen</span>
                                    <span>Produced</span>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </>

        );
    }
}
export default HomePageModal;