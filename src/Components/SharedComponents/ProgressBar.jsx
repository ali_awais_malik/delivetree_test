import React from 'react';

const progressBar = (props)=>{
    const {height, width, color, bgColor, total, current} = props;
    const percentage = (current / total )* 100
    console.log(`linear-gradient(to right, ${color} ${percentage}%, ${bgColor} 0)`)
    return(
        <>
    <div style = {{display: 'flex',padding:"0px 15px", justifyContent: 'space-between', height, width, background: `linear-gradient(to right, ${color} ${percentage}%, ${bgColor} 0)`, borderRadius: 15}}>
    <div style = {{flex:"100%", display: 'flex', justifyContent: 'space-between'}}><span>{current}</span><span>{total - current}</span></div>
    </div>
    <div className="font-small color-light" style = {{flex:"100%", display: 'flex', justifyContent: 'space-between',padding:"0px 15px"}}><span>Trees Planted</span><span>Available Capacity</span></div>
    </>
    )
}

export default progressBar;