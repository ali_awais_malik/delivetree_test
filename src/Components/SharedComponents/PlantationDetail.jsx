import React, { Component } from "react";
import MyMapComponent from "../Maps/Google-maps-react";
import StarRatings from "react-star-ratings";
import Progressbar from "./ProgressBar";
class PlantationDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leftBar: "0px",
            leftbarDisplay: "none",
            leftBarItems: "none",
            leftBarDetailWidth: "0px",
            leftBarDetailItems: "none",
            slideMenu: "0px",
            details: "none",
            showDetails: false,
            plantationDetailDisplay: "none"
        }
    }
    handleClick = () => {
        this.setState({ leftBar: "350px", leftBarItems: "block", details: "flex", leftbarDisplay: "block" });
    }
    handleSidebarDetailClick = () => {
        this.setState({ leftBarDetailWidth: this.state.leftBarDetailWidth === "0px" ? "calc(100vw - 367px)" : "0px", details: this.state.leftBarDetailItems === "none" ? "block" : "none", showDetails: this.state.showDetails ? false : true });
    }
    handleSidebarClose = () => {
        this.setState({ leftBar: "0", leftBarItems: "none" });
    }
    render() {
        const img = ['../media/eg.jpg', '../media/eg.jpg', '../media/eg.jpg', '../media/eg.jpg']
        return (
            <div style={{position:"relative", height:"100vh"}}>
                {/* left side panal opened when Plantation is clicked */}
                <div style={{ width: `${this.state.leftBar}`, float: "left", transition: "1s", position: "relative", zIndex: "1000", backgroundColor: "white", minHeight: "100vh", display: `${this.state.leftbarDisplay}`, flexDirection: "column" }}>
                    <div style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px", cursor: "pointer", fontSize: 18 }} onClick={() => { this.setState({ leftbarDisplay: "none", showDetails: false }) }}>X</div>
                    <div style={{ width: `${this.state.leftBar}`, transition: "1s", height: "180px" }}>
                        <img style={{ height: "100%", width: "100%" }} src={require("../../media/logo.png")} />
                    </div>
                    <div style={{ width: `${this.state.leftBar}`, alignItems: "baseLine", transition: "1s", display: `${this.state.details}`, flexDirection: "column", padding: "5% 7% 0% 7%" }}>
                        <h4>The Coca-Cola Plantation</h4>
                        <div style={{ display: "flex" }}>
                            <StarRatings
                                rating={4}
                                starRatedColor="var(--primary)"
                                numberOfStars={5}
                                name="rating"
                                starDimension={"18px"}
                                starSpacing={"1px"}
                            />
                            <p style={{ fontSize: "12px", marginLeft: "15px" }}>planted Tree:1270</p>
                        </div>
                        <p style={{ fontSize: "12px", color: "darkGrey" }}>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    </div>
                    <hr width="100%" style={{ margin: 0 }} />
                    <div style={{ width: `${this.state.leftBar}`, transition: "1s", display: `${this.state.details}`, flexDirection: "column", padding: "5% 7% 5% 7%" }}>
                        <h4>Plantation Sponsered By</h4>
                        <div style={{ display: "flex", justifyContent: "space-between" }}>
                            <img style={{ width: "90px" }} src={require("../../media/cocacola.png")} />
                            <img style={{ width: "90px" }} src={require("../../media/gov.png")} />
                            <img style={{ width: "90px" }} src={require("../../media/hbl.png")} />
                        </div>
                    </div>
                    <hr width="100%" style={{ margin: 0, backgroundColor: "lightGrey" }} />
                    <div style={{ width: `${this.state.leftBar}`, padding: "15px", display: `flex`, justifyContent: "center" }}>{!this.state.showDetails ? <button className="link-like-button" onClick={this.handleSidebarDetailClick}>View Details</button> : <i onClick={this.handleSidebarDetailClick} className="fa fa-arrow-left"></i>}</div>
                </div>

                {/* Expantion panal open when expand button clicked on left side panal */}
                {this.state.showDetails ?
                    <div style={{ width: `${this.state.leftBarDetailWidth}`, marginLeft: `350px`, float: "left", transition: "1s", position: "absolute", zIndex: "1000", backgroundColor: "white", height: "100vh", }}>
                        <div style={{ padding: "5%", display: "flex" }}>
                            <div style={{ flex: "60%", display: "flex", flexDirection: "column" }}>
                                <h4>Plantation Details</h4>
                                <div style={{ display: "flex", width: "100%", marginBottom: "0px" }}>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>
                                        <span className="font-small color-light">Land Sponsor</span>
                                        <p>Mian Muhammad Mansha</p>
                                    </div>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>
                                        <span className="font-small color-light">Monetary Sponsor</span>
                                        <p>Mian Muhammad Mansha</p>
                                    </div>
                                </div>
                                <div style={{ display: "flex", width: "100%", marginBottom: "0px" }}>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>
                                        <span className="font-small color-light">Media Partner</span>
                                        <p>Mian Muhammad Mansha</p>
                                    </div>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>
                                        <span className="font-small color-light">Up-keep Partner</span>
                                        <p>Mian Muhammad Mansha</p>
                                    </div>
                                </div>
                                <div style={{ display: "flex", width: "100%", marginBottom: "0px" }}>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>
                                        <span className="font-small color-light">Designed By</span>
                                        <p>Mian Muhammad Mansha</p>
                                    </div>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>

                                    </div>
                                </div>
                                <div style={{ display: "flex", width: "100%", marginBottom: "0px" }}>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>
                                        <span className="font-small color-light">Land Area</span>
                                        <p>Mian Muhammad Mansha</p>
                                    </div>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column" }}>
                                        <span className="font-small color-light">Total Capacity</span>
                                        <p>Mian Muhammad Mansha</p>
                                    </div>
                                </div>
                                <Progressbar total={100} current={34} color={'var(--secondary)'} bgColor={'#e9ecef'} width={'100%'} height={25} />
                                <span className="font-small" style={{ marginTop: "20px" }}>Location</span>
                                <p>Plot no 564, block h2, Johar Town, Lahore, Pakistan</p>
                                <div style={{ display: "flex", width: "100%", marginTop: "10px" }}>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "row", flexWrap: "wrap", justifyContent: "space-around" }}>
                                        {
                                            img.map(src => <img src={require('../../media/eg.png')} style={{ width: "45%", marginBottom: "12px", borderRadius: "4px", height: "110px" }} />)
                                        }
                                    </div>
                                    <div style={{ flex: "50%", display: "flex", flexDirection: "column", alignItems: "center", paddingLeft: "1.5%" }}>
                                        <img src={require('../../media/eg.png')} style={{ width: "100%", height: "62%", borderRadius: "4px" }} />
                                        <button style={{ width: "50%", backgroundColor: "var(--secondary)", marginTop: "10%", border: "none", height: "35px", borderRadius: "4px", color: "white" }}>Gallery</button>
                                    </div>
                                </div>
                            </div>

                            <div style={{ flex: "40%", padding: "5%" }}>
                                <div style={{ backgroundColor: "darkGrey", }}>
                                    fffffffffffffffffff
                            </div>

                            </div>
                        </div>
                    </div>
                    : null}
                <MyMapComponent onClick={this.handleClick} />
            </div>
        );
    }
}
export default PlantationDetail;