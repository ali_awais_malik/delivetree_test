import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./NavBar.css";
import { withRouter } from "react-router-dom";

class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchDispaly: "none",
            logoDivFlex: "40%",
            menuDivFlex: "40%",
            searchIcon: "inlineBlock"
        }
    }
 

    render() {
        return (
            <>
                <nav id="navbar">
                    <ul className="logo-div" style={{ flex: `${this.state.logoDivFlex}` }}>
                        <li className="navbar-menu">
                            <NavLink className={"navbar-link"} to="/">
                            <img src={require("../../media/logo.png")} />
                            </NavLink>
                        </li>
                    </ul>
                    <ul className="menu-div" style={{ flex: `${this.state.menuDivFlex}` }}>
                        <li className="navbar-menu"><NavLink className="active navbar-link" to="/about">About</NavLink></li>
                        <li className="navbar-menu"><NavLink className={"navbar-link"} to="/plantations">plantations</NavLink></li>
                        <li className="navbar-menu"><NavLink className={"navbar-link"} to="/articles">Articles</NavLink></li>
                        <li className="navbar-menu"><NavLink className={"navbar-link"} to="/contact">Contact</NavLink></li>
                        <li className="navbar-menu"><NavLink className={"navbar-link"} to="/signup">Signup/Login</NavLink></li>
                        <li className="navbar-menu"><button className="link-like-button" style={{color:"grey"}}><i className="fa fa-user"></i></button></li>
                    </ul>
                    <div className="for-flex"></div>
                </nav>

            </>
        );
    }
}

export default withRouter(NavBar);