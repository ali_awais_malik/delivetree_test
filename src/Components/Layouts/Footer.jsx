import React, {Component} from "react";
import "./Footer.css";

class Footer extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <>
            <div style={{position:"relative",padding:"3% 8%", display:"flex", flexDirection:"column", backgroundColor:"var(--secondary)",backgroundColor:"var(--secondary)", color:"white"}}>
                <div style={{display:"flex",width:"100%"}}>
                    <div style={{flex:"25%"}}>
                          <span style={{fontSize:30, fontWeight:"bolder"}}>DELIVETREE</span>
                    </div>
                    <div style={{flex:"25%"}}>
                         <ul>
                             <li className="footer-menu">About Us</li>
                             <li className="footer-menu">Contact</li>
                             <li className="footer-menu">Terms & Conditions</li>
                         </ul>
                    </div>
                    <div style={{flex:"25%"}}>
                    <ul>
                             <li className="footer-menu">+923451234567</li>
                             <li className="footer-menu">info@delivetree.com</li>
                         </ul>
                    </div>
                    <div style={{flex:"25%"}}>
                         <input type="email" style={{ border:"none",borderBottom:"1px solid #9c9999", width:"100%", backgroundColor:"var(--secondary)",color:"white"}} placeholder="Email"/>
                         <div style={{display:"flex", justifyContent:"space-between",marginTop:"10px"}}>
                             <span style={{fontSize:"12px", color:"white", flex:"50%"}}>Subscribe to our Newsletter</span>
                             <button style={{flex:"50%",width:"70px", height:"35px", backgroundColor:"white",color:"var(--secondary)", border:"none",borderRadius:"5px"}}>Subscribe</button>
                             </div>
                    </div>
                </div>
                <div style={{display:"flex"}}>
                    <div style={{flex:"15%", display:"flex", justifyContent:"space-between"}}>
                    <i className="fa fa-facebook fa-x"/>
                    <i className="fa fa-twitter fa-x"/>
                    <i className="fa fa-instagram fa-x"/>
                    </div>
                    <div style={{flex:"85%"}}>
                        <hr style={{width:"97%%", backgroundColor:"white", marginLeft:"3%"}}/>
                    </div>
                </div>
                <div style={{fontSize:"12px",display:"flex", justifyContent:"flex-end"}}>&copy; 2019 Delivetree, powered by Approcket
                    </div>
            </div>
            </>
        );
    }
}
export default Footer;