import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import HomePageModal from "../Home/HomePageModal";
import { Map, Polygon, GoogleApiWrapper, InfoWindow, Marker } from "google-maps-react";
import "./Google-maps-react.css";

class MyFancyComponent extends Component {
  constructor(props) {
    super(props);
    this.polygonRef = React.createRef();
    this.state = {
      isMarkerShown: false,
      coords: [],
      infos: [false, false, false, false],
      zoom: 6,
      center: { lat: 30.3753, lng: 69.3451 },
      points: []
    }
    this.handleZoomChanged.bind(this);
    this.handleZoomChanged.bind(this)
  }


  componentDidMount() {
    this.delayedShowMarker()
  }

  handleZoomChanged = (mapProps, map) => {
    this.setState({ zoom: map.getZoom(), center: { lat: map.getCenter().lat, lng: map.getCenter().lng } })
  }

  delayedShowMarker = () => {
    setTimeout(() => {
      this.setState({ isMarkerShown: true })
    }, 3000)
  }

  handleMarkerClick = () => {
    this.setState({ isMarkerShown: false })
    this.delayedShowMarker()
  }

  handleBounds = (mapProps, map) => {
    const { google } = this.props;
    const bounds = map.getBounds();
    let ne = bounds.getNorthEast();
    let sw = bounds.getSouthWest();
    let nw = new google.maps.LatLng(ne.lat(), sw.lng());
    let se = new google.maps.LatLng(sw.lat(), ne.lng());
    let boundries = [{ ne_lat: ne.lat(), ne_lng: ne.lng() }, { sw_lat: sw.lat(), sw_lng: sw.lng() }, { nw_lat: nw.lat(), nw_lng: nw.lng() }, { se_lat: se.lat(), se_lng: se.lng() }];
    let URL = "http://192.168.18.18:4000/near";
    const initObjet = {
      method: "post",
      headers: {
        Accept: "application/json",
        "content-type": "application/json"
      },
      body: JSON.stringify({
        nw: { lat: nw.lat(), lng: nw.lng() },
        sw: { lat: sw.lat(), lng: sw.lng() },
        se: { lat: se.lat(), lng: se.lng() },
        ne: { lat: ne.lat(), lng: ne.lng() }
      })
    }
    fetch(URL, initObjet)
      .then(response => response.json())
      .then(data => console.log(data))
  }


  handleClick = (i, mapProps, map) => {
    let URL = "http://192.168.18.18:4000/plantation/5dadb1f041ae3d073cd091de";
    fetch(URL, {method:"get"})
    .then(response=>response.json())
    .then(data=>console.log(data))
    this.props.onClick();
    this.setState({ infos: this.state.infos.map((info, j) => i === j ? !info : info) })
  }


  fetchCoords = () => {
    let URL = "http://192.168.18.18:4000/near";
    fetch(URL, { method: "post" })
      .then(response => response.json())
      .then(data => {
        const polygons = data['plantations']
        const result = polygons.map(({ loc: { coordinates } }) => {
          return coordinates[0].reduce((reduced, point, index) => {
            console.log(reduced, point, index)
            if (index !== coordinates[0].length - 1)
              return [...reduced, { lat: point[0], lng: point[1] }]
            return reduced
          }, [])
        })
        const markers = data['trees']
        const points = markers.map(({ loc: { coordinates } }) => ({ lat: coordinates[0], lng: coordinates[1] }))
        this.setState({ coords: result, points })
      })
      .catch(err => console.log(err))
  }
  componentWillMount() {
    this.fetchCoords();
  }

  render() {
    const { google } = this.props;
    return (
      <div>
        {this.state.center &&
          <Map
            google={this.props.google}
            className={"map"}
            
            // onZoom_changed={this.handleZoomChanged}
            // onIdle={this.handleBounds}
            // center={{ lat: this.state.center.lat, lng: this.state.center.lng }}
            // zoom={this.state.zoom}
          >
            {/* {
              this.state.coords.map((coords, i) => */}
                <Polygon
                  path={[{lat:31.471392, lng:74.352361},{lat:31.470219, lng:74.352898},{lat:31.470866, lng:74.351849},{lat:31.470587, lng:74.353158}]}
                  options={{
                    fillColor: "green",
                    fillOpacity: 0.4,
                    strokeColor: "green",
                    strokeOpacity: 0.5,
                    strokeWeight: 1
                  }}
          
                  onClick={() => this.handleClick()}
                />
            {/* } */}
            <Marker
              position={this.state.points[0]}
              onClick={() => this.handleClick()}
              icon={{
                url: "https://cdn3.iconfinder.com/data/icons/flat-icons-web/40/Tree_Alt-512.png",
                scaledSize: new google.maps.Size(32, 32)
              }}
            />

            {/* {
            this.state.coords.map((coords, i)=>
              <InfoWindow
              position={coords[0][1]}
              visible={this.state.infos[i]}
              content={`<div id="infoWindow"><br><p>${coords[1].name}</p></br><p>Hello i am a parahgraph</p></div>`}
            />)
              } */}
          </Map>}
          <HomePageModal/>
      </div>
    )
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyB1mDplUiN-c-T-wKCuMTeVCOY90Kamxi8"
})(withRouter(MyFancyComponent));