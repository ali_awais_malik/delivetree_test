import React, {Component} from 'react';
import Home from './Components/Home/Home';
import './App.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import PlantationDetail from "./Components/SharedComponents/PlantationDetail";

class App extends Component{
  constructor(props) {
    super(props);
    this.state = {

    }
  }
  render() {
    return (
    <>
    <Router>
      <Route path="/" exact component={Home}/>
      <Route path="/plantations" exact component={PlantationDetail}/>
    </Router>
      </>
      );
  }
}

export default App;
